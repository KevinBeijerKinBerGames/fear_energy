﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonFunction : MonoBehaviour {
	//This sets the buttons from both the main menu and the end menu

	public void PlayAgain(){
		SteamVR_LoadLevel.Begin("Corridor");
    }

    public void Quit(){
        Application.Quit();
    }
}
