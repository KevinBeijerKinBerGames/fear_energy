﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Loading : MonoBehaviour
{
    private int n = 0;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Loading trigger")
        {
            SteamVR_LoadLevel.Begin("Room");
        }

        if (other.tag == "Audio que")
        {
            n++;

            other.GetComponent<AudioSource>().Play();
            if (n == 2)
            {
                gameObject.GetComponent<AudioSource>().Stop();
            }
        }
    }
}
