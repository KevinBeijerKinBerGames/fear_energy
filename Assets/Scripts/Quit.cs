﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quit : MonoBehaviour {

	void OnTriggerEnter(Collider other)
	{
		Application.Quit();
	}
}
