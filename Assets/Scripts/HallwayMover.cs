﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HallwayMover : MonoBehaviour
{
    public Rigidbody rb;

    private Vector3 targetAngle;

    private Vector3 currentAngle;

    void Start()
    {
        rb = GetComponent<Rigidbody>();

        currentAngle = transform.eulerAngles;
    }

    void FixedUpdate()
    {
        if (transform.position.z < 8)
        {
            rb.MovePosition(transform.position + transform.forward * Time.deltaTime);
        }
        if (transform.position.x < 7 && transform.position.z > 8)
        {
            rb.MovePosition(transform.position + transform.right * Time.deltaTime);
            if (transform.rotation.y != 90)
            {
                targetAngle = new Vector3(0f, 90f, 0f);

                currentAngle = new Vector3(
                 Mathf.LerpAngle(currentAngle.x, targetAngle.x, Time.deltaTime),
                 Mathf.LerpAngle(currentAngle.y, targetAngle.y, Time.deltaTime),
                 Mathf.LerpAngle(currentAngle.z, targetAngle.z, Time.deltaTime));

                transform.eulerAngles = currentAngle;
            }
        }
        if (transform.position.x > 7)
        {
            rb.MovePosition(transform.position + transform.forward * Time.deltaTime);
            if (true)
            {
                targetAngle = new Vector3(0f, 0f, 0f);

                currentAngle = new Vector3(
                 Mathf.LerpAngle(currentAngle.x, targetAngle.x, Time.deltaTime),
                 Mathf.LerpAngle(currentAngle.y, targetAngle.y, Time.deltaTime),
                 Mathf.LerpAngle(currentAngle.z, targetAngle.z, Time.deltaTime));

                transform.eulerAngles = currentAngle;
            }
        }
    }
}
